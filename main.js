import Vue from 'vue'
import App from './App'
import http from './components/http/http.js'
Vue.config.productionTip = false
Vue.prototype.$api = http.http
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
