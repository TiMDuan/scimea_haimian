const urls = 'http://api.haimiantv.cn';

/* import http from '@/components/http.js'
// get 请求
http.get(URL, [params]).then((res) => {
    **
}).catch(error => {
    **
}).finally(() => {
    **
})
 
// post 请求
http.post(URL, [data]).then(res => {

}).catch(error => {

}).finally(() => {

}) */

var ImgUrl = urls + '/portal/index/uploader'

const http = function(url, data, success, fail) {
	uni.showLoading({
		title:'加载中'
	})
	if (fail == undefined || fail == '') {
		fail = function() {

		}
	}

	var methods = data.methods ? data.methods : 'GET',
		datas = data.datas
		
	var token = uni.getStorageSync('Token')

	var headers = (methods == 'GET') ? {
		'Authori-zation': 'Bearer ' + token.token,
		'Authorization': 'Bearer ' + token.token,
		'X-Requested-With': 'XMLHttpRequest',
		"Accept": "application/json",
		"Content-Type": "multipart/form-data",
		"Content-Type": "application/json; charset=UTF-8"
	} : {
		'Authori-zation': 'Bearer ' + token.token,
		'Authorization': 'Bearer ' + token.token,
		'X-Requested-With': 'XMLHttpRequest',
		"Content-Type": "multipart/form-data",
		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	}
	
	uni.request({
		url: urls + url,
		data: datas || {},
		header: headers,
		sslVerify: false,
		dataType: 'json',
		method: methods || 'GET',
		success: function(res) {
			if (res.data.code == 401) {
				uni.showToast({
					title: '登陆过期，您需要重新登陆',
					icon: 'none',
					duration: 1500
				})
				setTimeout(function() {
					uni.reLaunch({
						url: '../kong/kong'
					})
				}, 1500)
			
			}
			if(res.data.msg =="请登录后在操作"){
				uni.reLaunch({
					url:'/pages/kongs/kongs'
				})
				return false
			}
			
			success(res)
			uni.hideLoading()
		},
		fail: function(res) {
			fail(res)
			uni.hideLoading()
		}
	})
}

export default {
	http: http,
	urls: urls,
	ImgUrl: ImgUrl
}